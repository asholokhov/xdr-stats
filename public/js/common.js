$(document).ready(function(){
    // process links
    var currentPath = window.location.pathname;
    $('.nav')
        .find('a')
        .each(function() {
            if ($(this).attr('href') === currentPath) {
                $(this)
                    .parent()
                    .addClass('active');
            }
        });

    // init datetime pickers
    $('#btime, #etime').datetimepicker({
        format: 'dd.mm.yyyy hh:ii'
    });

    if (currentPath === '/') {
        // load cdpns from server
        $.ajax({
            type: 'GET',
            url: 'cdpnList',
            dataType: 'json'
        }).done(function(data) {
            $('#cdpns').html("");
            $.each(data, function(i, item) {
                var t = item.cdpn.replace('+', '');
                $('#cdpns').append("<option>" + t + "</option>");
            });
        }).fail(function() {
            console.log('Cant load cdpns list');
        });

        // start button
        $('#rock-btn').on('click', function() {
            var btn = $(this);
            btn.button('loading');

            $.ajax({
                type: 'GET',
                url: '/getstats',
                data: {
                    btime: $('#btime').val(),
                    etime: $('#etime').val(),
                    cdpn:  $('#cdpns').val()
                }
            }).done(function(data) {
                $('.main-stat').html(data);

                // charts
                drawPoolDynamicChart();
                drawOperatorsDynamicChart();
            }).fail(function() {
                console.log('Collecting data failed');
            }).always(function() {
                btn.button('reset');
            });
        });
    }

    if (currentPath === '/builder') {
        // generation button
        $('#build-btn').on('click', function() {
            var btn = $(this);
            btn.button('loading');

            $.ajax({
                type: 'GET',
                url: '/builder/make',
                data: {
                    count: $('#xdr-count').val(),
                    btime: $('#btime').val(),
                    etime: $('#etime').val(),
                    cdpn:  $('#cdpn').val()
                }
            }).done(function (data) {
                $("#xdrs-list").html(data);
            }).fail(function(jqXHR, textStatus, errorThrown)  {
                console.log(textStatus, errorThrown);
                throw errorThrown;
            }).always(function() {
                btn.button('reset');
            });
        });

        // clear db
        $('#clear-bd').on('click', function() {
            var btn = $(this);
            btn.button('loading');

            $.ajax({
                type: 'GET',
                url: '/builder/clear'
            }).always(function() {
                btn.button('reset');
            });
        });
    }

});