function drawPoolDynamicChart() {
    $.ajax({
        url: '/pool_json',
        dataType: 'json'
    }).done(function(json) {
        var categories = [];
        var data = [];

        for (var i = 0; i < json.length; i++) {
            categories.push(json[i].time);
            data.push(json[i].value);
        }

        $('#pool-dyn').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Динамика размера очереди'
            },
            subtitle: {
                text: 'для выбранной группы операторов'
            },
            xAxis: {
                categories: categories,
                labels: {
                    enabled: false
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Количество вызовов (шт)'
                }
            },
            series: [{
                name: 'Количество вызовов в очереди',
                data: data
            }]
        });

    }).fail(function() {
        console.log('draw pool chart failed');
    });
}

function drawOperatorsDynamicChart() {
    $.ajax({
        url: '/oper_json',
        dataType: 'json'
    }).done(function(json) {
        var categories = [];
        var data = [];

        for (var i = 0; i < json.length; i++) {
            categories.push(json[i].time);
            data.push(json[i].value);
        }

        $('#oper-dyn').highcharts({
            chart: {
                type: 'column'
            },
            title: {
                text: 'Динамика нагрузки на операторов'
            },
            subtitle: {
                text: 'для выбранной группы операторов'
            },
            xAxis: {
                categories: categories,
                labels: {
                    enabled: false
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Количество активных вызовов (шт)'
                }
            },
            series: [{
                name: 'Количество активных вызовов',
                data: data
            }]
        });

    }).fail(function() {
        console.log('draw oper chart failed');
    });
}