var moment = require('moment');
var _ = require('underscore');
var db = require('./db');

var cached_pool_dyn = [];
var cached_oper_dyn = [];

function avg(list) {
    if (list.length === 0)
        return 0;

    var s = 0;
    for (var i = 0; i < list.length; i++)
        s += list[i].value;

    return (s / list.length);
}

exports.collectStats = function(params, cb) {
    var dt_format = 'DD.MM.yyyy HH:mm:ss';

    var b_time = moment(params.btime, dt_format).unix();
    var e_time = moment(params.etime, dt_format).unix();

    var stats = {};
    var err   = null;

    var query = '';

    // get all cdrs with correct start time
    query = 'SELECT *, (answer_time - start_time) AS queue_duration FROM cdrs' +
            ' WHERE (start_time + (answer_time - start_time) + conversation > ?) ' +
            ' AND (start_time < ?) ORDER BY start_time ASC';

    db.query(query, [b_time, e_time], function(err, xdrs) {
        if (xdrs.length == 0) {
            cached_pool_dyn = [];
            cached_oper_dyn = [];
            cb(null, {});
            return;
        }

        var p_start = xdrs[0].start_time;

        var pool_size = 0;
        var last_ps   = 0;
        var pool_sizes = [];

        var op_busy = 0;
        var op_last = 0;
        var op_pool = [];

        for (var r = p_start; r <= e_time; r++) {
            for (var j = 0; j < xdrs.length; j++) {
                pool_size += (xdrs[j].start_time  == r) ? 1 : 0;

                if (xdrs[j].answer_time == r) { // operator answered
                    pool_size--;
                    op_busy++;
                }

                if (xdrs[j].answer_time + xdrs[j].conversation == r) { // end call
                    op_busy--;
                }

            }

            var time = moment(r * 1000).format("DD.MM HH:mm");

            if (op_busy != op_last) {
                op_pool.push(_.object(['time', 'value'], [time, op_busy]));
                op_last = op_busy;
            }

            if (pool_size != last_ps) {
                pool_sizes.push(_.object(['time', 'value'], [time, pool_size]));
                last_ps = pool_size;
            }
        }

        // pool
        stats.max_size = _.max(pool_sizes, function(el) { return el.value; });
        stats.avg_size = avg(pool_sizes).toFixed(2);

        // operators
        stats.op_b_max = _.max(op_pool, function(el) { return el.value; });

        // xdrs list
        var m_index = 0;
        var conv_summary = 0;

        for (var j = 0; j < xdrs.length; j++) {
            xdrs[j].start_time  = moment(xdrs[j].start_time * 1000).format("DD.MM HH:mm");
            xdrs[j].answer_time = moment(xdrs[j].answer_time * 1000).format("DD.MM HH:mm");

            conv_summary += xdrs[j].conversation;

            if (xdrs[j].queue_duration > xdrs[m_index].queue_duration) {
                m_index = j;
            }
        }

        xdrs[m_index].is_max = true;
        stats.xdrs_lst = xdrs;
        stats.erlangs  = (conv_summary / (e_time - b_time)).toFixed(2);

        // save for charts
        cached_pool_dyn = pool_sizes;
        cached_oper_dyn = op_pool;

        console.log('Stats collected');

        if ('undefined' !== typeof cb) {
            cb(err, stats);
            return;
        }

        if (err) throw err;
    });
};

exports.getPoolJSON = function(cb) {
    cb(JSON.stringify(cached_pool_dyn));
};

exports.getOperJSON = function(cb) {
    cb(JSON.stringify(cached_oper_dyn));
};