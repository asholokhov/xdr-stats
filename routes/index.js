var express = require('express');
var router = express.Router();
var db = require('./db');
var stats = require('./stats');

/*

Среднее время в очереди:
 select AVG(answer_time - start_time) as time_in_pool from cdrs;

 */

/* GET home page. */
router.get('/', function(req, res) {
    res.render('index', { title: 'Протей | Статистика' });
});

router.get('/cdpnList', function(req, res) {
    db.getCdpnList(function(err, result) {
        if (err) console.log(err);
        res.send(result);
    });
});

router.get('/getstats', function(req, res) {
    stats.collectStats(req.query, function(err, stats) {
        if (!stats.hasOwnProperty('max_size')) {
            res.send('<h4>Данных нет.</h4>');
        } else {
            res.render('stats', { stats: stats });
        }
    })
});

router.get('/pool_json', function(req, res) {
    stats.getPoolJSON(function(json) {
        res.send(json);
    });
});

router.get('/oper_json', function(req, res) {
    stats.getOperJSON(function(json) {
        res.send(json);
    });
});

module.exports = router;
