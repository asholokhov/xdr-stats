var mysql = require('mysql');
var db = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: 'halflife',
    database: 'protei'
});

db.connect(function(err) {
    if (err) throw err;

    // connection successful
    console.log('Connection to DB established.');
});

exports.query = function(sql, params, cb) {
    db.query(sql, params, cb);
};

exports.getCdpnList = function(cb) {
    var query = 'SELECT DISTINCT cdpn FROM cdrs;';
    db.query(query, function(err, res) {
        cb(err, res);
    });
};

exports.saveXdrs = function(xdrs, cb) {
    var query = 'INSERT INTO cdrs (start_time, answer_time, conversation, cgpn, cdpn) VALUES (?, ?, ?, ?, ?);';
    for (var i = 0; i < xdrs.length; i++) {
        db.query(query, [xdrs[i].s_time, xdrs[i].a_time, xdrs[i].conv, xdrs[i].cgpn, xdrs[i].cdpn], function(err, result) {
            if (err) throw err;
        });
    }

    var err = null;
    if ('undefined' !== typeof cb) {
        cb(err);
    }
};
