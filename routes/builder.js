var express = require('express');
var moment = require('moment');
var _ = require('underscore');
var db = require('./db');

var router = express.Router();

function buildXdrs(params, cb) {
    var cgpns = [ // calling group party numbers
        79262083892, 79261789049, 79264763696, 79263657325,
        79261036675, 79261804825, 79262439477, 79265299402,
        79263328043, 79264107492, 79263132423, 79261018254,
        79262239065, 79264864311, 79262329234, 79264240967,
        79261946221, 79263890722, 79262952636, 79263024586,
        79262199211, 79261793631, 79264965539, 79264151177,
        79261825668, 79264318789, 79263556556, 79261860918,
        79261599592, 79263578262, 79263515411, 79264293800,
        79264980542, 79261822095, 79263801730, 79261172491
    ];

    var conversation_max_t = 1000;

    var dt_format = 'DD.MM.yyyy HH:mm:ss';

    var count  = params.count;
    var b_time = moment(params.btime, dt_format).unix();
    var e_time = moment(params.etime, dt_format).unix();
    var cdpn   = params.cdpn;

    var s_time;
    var a_time;
    var cgpn;
    var conv;

    var xdrs = [];
    for (var i = 0; i < count; i++) {
        cgpn = cgpns[_.random(0, cgpns.length - 1)];
        s_time = _.random(b_time, e_time);
        a_time = _.random(s_time, e_time);
        conv = _.random(conversation_max_t);

        xdrs.push(_.object(
            ['s_time', 'a_time', 'conv', 'cgpn', 'cdpn', 's_time_de', 'a_time_de'],
            [s_time, a_time, conv, cgpn, cdpn, moment(s_time*1000).format("DD.MM.YYYY hh:mm:ss"),
                moment(a_time*1000).format("DD.MM.YYYY hh:mm:ss")]
        ));
    }

    var err = null;
    if ('undefined' !== typeof cb) {
        cb(xdrs, err);
    }
}

/* GET builder listing. */
router.get('/', function(req, res) {
    res.render('builder', { title: 'Протей | Генерация xDR' });
});

router.get('/make', function(req, res) {
    buildXdrs(req.query, function(xdrs, err) {
        if (err) throw err;
        db.saveXdrs(xdrs);
        res.render('xdr_list', { xdrs: xdrs });
    });
});

router.get('/clear', function(req, res) {
    db.query('DELETE FROM cdrs;', function(err, result) {
        console.log('DB cleared');
        if (err) throw err;
        res.send('ok');
    });
});

module.exports = router;
